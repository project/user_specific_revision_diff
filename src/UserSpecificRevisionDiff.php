<?php

namespace Drupal\user_specific_revision_diff;

use Caxy\HtmlDiff\HtmlDiffConfig;

/**
 * Class UserSpecificRevisionDiff.
 *
 * @package Drupal\user_specific_revision_diff
 */
class UserSpecificRevisionDiff extends \HtmlDiffAdvanced {

  /**
   * This variable stores class to added to ins tag for inserted text.
   *
   * @var string
   */
  protected $insertOperationClass;

  /**
   * This function is used to set diff class for particular revision.
   *
   * @param string $class
   *   Class string.
   */
  public function setInsertOperationClass($class) {
    $this->insertOperationClass = $class;
  }

  /**
   * {@inheritdoc}
   */
  protected function performOperation($operation) {
    switch ($operation->action) {
      case 'equal':
        $this->processEqualOperation($operation);
        break;

      case 'delete':
        $this->processDeleteOperation($operation, 'diffdel');
        break;

      case 'insert':
        $this->processInsertOperation($operation, $this->insertOperationClass);
        break;

      case 'replace':
        $this->processReplaceOperation($operation);
        break;

      default:
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function processReplaceOperation($operation) {
    $this->processDeleteOperation($operation, 'diffmod');
    $this->processInsertOperation($operation, $this->insertOperationClass);
  }

  /**
   * {@inheritdoc}
   */
  protected function diffElements($oldText, $newText, $stripWrappingTags = TRUE) {
    $wrapStart = '';
    $wrapEnd = '';

    if ($stripWrappingTags) {
      $pattern = '/(^<[^>]+>)|(<\/[^>]+>$)/iu';
      $matches = [];

      if (preg_match_all($pattern, $newText, $matches)) {
        $wrapStart = isset($matches[0][0]) ? $matches[0][0] : '';
        $wrapEnd = isset($matches[0][1]) ? $matches[0][1] : '';
      }
      $oldText = preg_replace($pattern, '', $oldText);
      $newText = preg_replace($pattern, '', $newText);
    }

    $diff = self::create($oldText, $newText, $this->config);

    return $wrapStart . $diff->build() . $wrapEnd;
  }

  /**
   * {@inheritdoc}
   */
  public static function create($oldText, $newText, HtmlDiffConfig $config = NULL) {
    $diff = new self($oldText, $newText);

    if (NULL !== $config) {
      $diff->setConfig($config);
    }

    return $diff;
  }

  /**
   * {@inheritdoc}
   */
  protected function insertTag($tag, $cssClass, &$words) {
    while (TRUE) {
      if (count($words) == 0) {
        break;
      }

      $nonTags = $this->extractConsecutiveWords($words, 'noTag');

      $specialCaseTagInjection = '';
      $specialCaseTagInjectionIsBefore = FALSE;

      if (count($nonTags) != 0) {
        $text = $this->wrapText(implode('', $nonTags), $tag, $cssClass);
        $this->content .= $text;
      }
      if (count($words) == 0 && mb_strlen($specialCaseTagInjection) == 0) {
        break;
      }
      if ($specialCaseTagInjectionIsBefore) {
        $this->content .= $specialCaseTagInjection . implode('', $this->extractConsecutiveWords($words, 'tag'));
      }
      else {
        $workTag = $this->extractConsecutiveWords($words, 'tag');
        $appendContent = implode('', $workTag) . $specialCaseTagInjection;
        if (isset($workTag[0]) && FALSE !== mb_stripos($workTag[0], '<img')) {
          $appendContent = $this->wrapText($appendContent, $tag, $cssClass);
        }
        $this->content .= $appendContent;
      }
    }
  }

}
