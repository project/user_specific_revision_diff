<?php

namespace Drupal\user_specific_revision_diff\Plugin\diff\Layout;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\diff\Controller\PluginRevisionController;
use Drupal\diff\Plugin\diff\Layout\VisualInlineDiffLayout;
use Drupal\user_specific_revision_diff\UserSpecificRevisionDiff;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VisualInlineDiffLayoutCustom.
 *
 * @package Drupal\user_specific_revision_diff\Plugin\diff\Layout
 */
class VisualInlineDiffLayoutCustom extends VisualInlineDiffLayout {


  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The user specific revision diff diff service.
   *
   * @var \Drupal\user_specific_revision_diff\UserSpecificRevisionDiff
   */
  protected $userSpecificRevisionDiff;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setRouteMatch($container->get('current_route_match'));
    $instance->setUserSpecificRevisionDiff($container->get('user_specific_revision_diff.html_diff'));
    return $instance;
  }

  /**
   * Sets route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route Match service object.
   */
  public function setRouteMatch(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * Sets User Specific Revision Diff service.
   *
   * @param \Drupal\user_specific_revision_diff\UserSpecificRevisionDiff $user_specific_revision_diff
   *   UserSpecificRevisionDiff service object.
   */
  public function setUserSpecificRevisionDiff(UserSpecificRevisionDiff $user_specific_revision_diff) {
    $this->userSpecificRevisionDiff = $user_specific_revision_diff;
  }

  /**
   * {@inheritdoc}
   */
  public function build(ContentEntityInterface $left_revision, ContentEntityInterface $right_revision, ContentEntityInterface $entity) {
    // Build the revisions data.
    $build = $this->buildRevisionsData($left_revision, $right_revision);

    $this->entityTypeManager->getStorage($entity->getEntityTypeId())->resetCache([$entity->id()]);
    // Build the view modes filter.
    $options = [];
    // Get all view modes for entity type.
    $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle($entity->getEntityTypeId(), $entity->bundle());
    foreach ($view_modes as $view_mode => $view_mode_info) {
      // Skip view modes that are not used in the front end.
      if (in_array($view_mode, ['rss', 'search_index'])) {
        continue;
      }
      $options[$view_mode] = [
        'title' => $view_mode_info,
        'url' => PluginRevisionController::diffRoute($entity,
          $left_revision->getRevisionId(),
          $right_revision->getRevisionId(),
          'visual_inline',
          ['view_mode' => $view_mode]
        ),
      ];
    }

    $active_option = array_keys($options);
    $active_view_mode = $this->requestStack->getCurrentRequest()->query->get('view_mode') ?: reset($active_option);

    $filter = $options[$active_view_mode];
    unset($options[$active_view_mode]);
    array_unshift($options, $filter);

    $build['controls']['view_mode'] = [
      '#type' => 'item',
      '#title' => $this->t('View mode'),
      '#wrapper_attributes' => ['class' => 'diff-controls__item'],
      'filter' => [
        '#type' => 'operations',
        '#links' => $options,
      ],
    ];

    $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());

    $entity_type_id = $left_revision->getEntityTypeId();
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->routeMatch->getParameter($entity_type_id);
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $all_revision_ids = $this->getRevisionIds($storage, $entity->id());
    $left_revision_id_position = array_keys($all_revision_ids, $left_revision->getRevisionId())[0];
    $right_revision_id_position = array_keys($all_revision_ids, $right_revision->getRevisionId())[0];
    $offset = min($left_revision_id_position, $right_revision_id_position) + 1;
    $length = abs($left_revision_id_position - $right_revision_id_position) - 1;
    $required_revision_ids = array_slice($all_revision_ids, $offset, $length);
    // Trigger exclusion of interactive items like on preview.
    if ($left_revision_id_position < $right_revision_id_position) {
      $first_revision = $left_revision;
      $last_revision = $right_revision;
    }
    else {
      $first_revision = $right_revision;
      $last_revision = $left_revision;
    }
    $revision_classes = [
      'user-minion',
      'user-apple',
      'user-brilliant-rose',
      'user-summer-sky',
      'user-sunset',
      'user-parrot',
      'user-teal',
      'user-cerulean',
      'user-prussian-blue',
      'user-midnight',
      'user-cloud',
    ];
    $first_revision->in_preview = TRUE;
    $left_view = $view_builder->view($first_revision, $active_view_mode);
    unset($left_view['#cache']);
    $html_1 = $this->renderer->render($left_view);
    $this->userSpecificRevisionDiff->setOldHtml('');
    $this->userSpecificRevisionDiff->setNewHtml($html_1);
    $first_revision_class = array_shift($revision_classes);
    $this->userSpecificRevisionDiff->setInsertOperationClass($first_revision_class);
    $user_html = '';
    $user_html .= '<div class="' . $first_revision_class . '">' . $first_revision->getRevisionUser()->name->value . '</div>';
    $this->userSpecificRevisionDiff->build();
    $diff = $this->userSpecificRevisionDiff->getDifference();
    $this->userSpecificRevisionDiff->setOldHtml($diff);
    foreach ($required_revision_ids as $revision_id) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $revision */
      $revision = $storage->loadRevision($revision_id);
      $revision->in_preview = TRUE;
      $revision_view = $view_builder->view($revision, $active_view_mode);
      unset($revision_view['#cache']);
      $html = $this->renderer->render($revision_view);
      $this->userSpecificRevisionDiff->setNewHtml($html);
      $revision_class = array_shift($revision_classes);
      $this->userSpecificRevisionDiff->setInsertOperationClass($revision_class);
      $user_html .= '<div class="' . $revision_class . '">' . $revision->getRevisionUser()->name->value . '</div>';
      $this->userSpecificRevisionDiff->build();
      $diff = $this->userSpecificRevisionDiff->getDifference();
      $this->userSpecificRevisionDiff->setOldHtml($diff);
    }

    $last_revision->in_preview = TRUE;
    $right_view = $view_builder->view($last_revision, $active_view_mode);
    // Avoid render cache from being built.
    unset($right_view['#cache']);
    $html_2 = $this->renderer->render($right_view);
    $this->userSpecificRevisionDiff->setNewHtml($html_2);
    $last_revision_class = array_shift($revision_classes);
    $this->userSpecificRevisionDiff->setInsertOperationClass($last_revision_class);
    $user_html .= '<div class="' . $last_revision_class . '">' . $last_revision->getRevisionUser()->name->value . '</div>';
    $this->userSpecificRevisionDiff->build();

    $build['diff'] = [
      '#markup' => $user_html . '<br>' . $this->userSpecificRevisionDiff->getDifference(),
      '#weight' => 10,
    ];

    $build['#attached']['library'][] = 'user_specific_revision_diff/user_specific_revision_diff.visual_inline';
    return $build;
  }

  /**
   * Get all revision ids for a node.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage manager.
   * @param int $entity_id
   *   The entity to find revisions of.
   *
   * @return int[]
   *   The revision ids.
   */
  public function getRevisionIds(EntityStorageInterface $storage, $entity_id) {
    $result = $storage->getQuery()
      ->allRevisions()
      ->condition($storage->getEntityType()->getKey('id'), $entity_id)
      ->accessCheck(FALSE)
      ->execute();
    $result_array = array_keys($result);
    sort($result_array);
    return $result_array;
  }

}
