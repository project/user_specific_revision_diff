This module is dependent on existing diff module and is an extension of that.

This module

By default it provides 11 colors and repeats them for more than 11 users for this initial release.

User Specific Revision Diff module for Drupal 8
-------------------------------------

This module is dependent on existing diff module and is an extension of that.
It allows the user to compare revisions with Visual inline diff layout providing
ability to visually attribute changes from each user per revision who authored
them (e.g. using different colors per user along with their name).

Requirements
------------------

To use User Specific Revision Diff you need to install and activate the
following modules:

 * Entity
 * diff

And the following libraries dependencies:
 * php-htmldiff-advanced (Used for displaying the comparison as a rendered
   entity)

